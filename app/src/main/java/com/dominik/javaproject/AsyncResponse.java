package com.dominik.javaproject;

import org.json.JSONArray;

/**
 * Created by Dominik on 2017-06-04.
 */

public interface AsyncResponse {
    void processFinish(JSONArray jsonObject);
}
