package com.dominik.javaproject;

import org.json.JSONArray;

/**
 * Created by Dominik on 2017-06-07.
 */

public interface AsyncResponseLectures {
    void processFinish(JSONArray jsonObject);

}
