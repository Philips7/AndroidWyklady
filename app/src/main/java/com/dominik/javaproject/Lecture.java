package com.dominik.javaproject;

import java.util.Date;

/**
 * Created by Dominik on 2017-06-07.
 */

public class Lecture {

    private Integer lectureId;
    private String teacherName;
    private String teacherSurname;
    private String title;
    private Date date;

    public Lecture() {
    }

    public Lecture(Integer lectureId, String teacherName, String teacherSurname, String title, Date date) {
        this.lectureId = lectureId;
        this.teacherName = teacherName;
        this.teacherSurname = teacherSurname;
        this.title = title;
        this.date = date;
    }

    public Integer getLectureId() {
        return lectureId;
    }

    public void setLectureId(Integer lectureId) {
        this.lectureId = lectureId;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getTeacherSurname() {
        return teacherSurname;
    }

    public void setTeacherSurname(String teacherSurname) {
        this.teacherSurname = teacherSurname;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
