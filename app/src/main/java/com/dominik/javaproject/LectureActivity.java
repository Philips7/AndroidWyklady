package com.dominik.javaproject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.PowerManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.github.barteksc.pdfviewer.PDFView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import layout.Pdf;

public  class LectureActivity extends AppCompatActivity implements AsyncResponse{

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private ArrayList<Video> videoArrayList = null;
    private ProgressDialog mProgressDialog;
    Intent videoPlaybackActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lecture);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateLectures();
                Snackbar.make(view, "Uaktualniam wykład. Prosze czekać", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

            }
        });






//        pdfView = (PDFView) findViewById(R.id.pdfView);
//        pdfView.fromAsset("angular.pdf");

        videoPlaybackActivity = null;
        final LecturesTask lecturesTask = new LecturesTask(LectureActivity.this);
        lecturesTask.delegate = this;
        lecturesTask.execute("https://pacific-ocean-71911.herokuapp.com/videos");
    }

    private void updateLectures() {
        final LecturesTask lecturesTask = new LecturesTask(LectureActivity.this);
        lecturesTask.delegate = this;
        lecturesTask.execute("https://pacific-ocean-71911.herokuapp.com/videos");
        LinearLayout mainLayout = (LinearLayout) findViewById(R.id.videoLinearLayout);
        mainLayout.removeAllViews();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_lecture, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void processFinish(JSONArray jsonObject) {
        System.out.println(jsonObject.toString());
        ArrayList<Video> videos = null;
        if (jsonObject != null) {
            videos = jsonArrayToVideos(jsonObject);
            videoArrayList = jsonArrayToVideos(jsonObject);
        }

        if (videos != null) {
            LinearLayout mainLayout = (LinearLayout) findViewById(R.id.videoLinearLayout);
            int viewCounter = 6;
            int cardViewCounter = videos.size();
            generateCards(mainLayout, viewCounter, cardViewCounter, videos);
        }
    }


    private ArrayList<Video> jsonArrayToVideos(JSONArray jsonArrayResponse) {
        ArrayList<Video> videos = new ArrayList<>();
        String pattern = "MM/dd/yyyy";
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        Intent intent = getIntent();
        String lectureId = intent.getStringExtra("lectureId");
        Integer id = Integer.parseInt(lectureId);

        for (int i = 0; i < jsonArrayResponse.length(); i++) {
            try {
                JSONObject object = (JSONObject) jsonArrayResponse.get(i);
                if (object.getInt("lectureId") == id){
                    Video video = new Video();
                    video.setVideoId(object.getInt("videoId"));
                    video.setFilename(object.getString("filename"));
                    video.setDescription(object.getString("description"));
                    video.setExtension(object.getString("extension"));
                    video.setLectureId(object.getInt("lectureId"));
                    Date date = format.parse("12/31/2006");
                    video.setDate(date);
                    videos.add(video);
                }

            } catch (JSONException ex) {
                Log.d("MAIN", ex.getMessage());
            } catch (ParseException ex) {
                Log.d("MAIN", ex.getMessage());
            }
        }
        return videos;
    }

    private void generateCards(LinearLayout mainLayout, int viewCounter, int cardViewCounter, ArrayList<Video> videos) {

        Integer[] drawables = new Integer[3];
        drawables[0] = R.drawable.video_lecture;
        drawables[1] = R.drawable.video_lecture;
        drawables[2] = R.drawable.video_lecture;


        for (int id = 0, j = 0; id < viewCounter * cardViewCounter; id += viewCounter, j++) {
            String teacher = videos.get(j).getDescription();
            String title = videos.get(j).getFilename();
            String date = videos.get(j).getDate().toString();
            Integer lectureId = videos.get(j).getLectureId();
            Integer videoId = videos.get(j).getVideoId();
            createCardView(mainLayout, id, title, teacher, date, drawables[j], lectureId, videoId);
        }
    }


    private void createCardView(LinearLayout mainLinearLayout, int id, String title, String teacher, String date, Integer drawable, Integer lectureId, Integer videoId) {

        CardView cardView = new CardView(this);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(
                ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.MATCH_PARENT
        );
        cardView.setLayoutParams(params);
        cardView.setCardBackgroundColor(Color.parseColor("#FFC6D6C3"));
        cardView.setId(id);
        RelativeLayout cardViewRelativeLayout = new RelativeLayout(this);
        ActionBar.LayoutParams relativeParams = new ActionBar.LayoutParams(
                ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.MATCH_PARENT
        );
        cardView.setContentPadding(15, 15, 15, 15);
        cardView.setMaxCardElevation(15);
        cardView.setCardElevation(9);
        cardView.addView(cardViewRelativeLayout, relativeParams);

        addImageView(id, cardViewRelativeLayout, drawable, lectureId, videoId);
        addTextViewTitle(id, cardViewRelativeLayout, title);
        addTextViewTeacher(id, cardViewRelativeLayout, teacher);
        addTextViewData(id, cardViewRelativeLayout, date);
        addTextViewMore(id, cardViewRelativeLayout, "More...");

        mainLinearLayout.addView(cardView);
    }

    private void addTextViewTitle(int id, RelativeLayout cardViewRelativeLayout, String title) {
        TextView tv = new TextView(this);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(
                ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.MATCH_PARENT
        );
        tv.setLayoutParams(params);
        tv.setText(title);
        tv.setPadding(10, 0, 20, 0);
        tv.setTextColor(Color.DKGRAY);
        tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        tv.setTypeface(Typeface.DEFAULT_BOLD);
        tv.setId(id + 5);
        RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        p.addRule(RelativeLayout.BELOW, id + 1);
        p.addRule(RelativeLayout.CENTER_HORIZONTAL, 1);
        p.addRule(RelativeLayout.CENTER_VERTICAL, 1);
        cardViewRelativeLayout.addView(tv, p);
    }

    private void addTextViewMore(int id, RelativeLayout cardViewRelativeLayout, String more) {
        TextView tv = new TextView(this);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(
                ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.MATCH_PARENT
        );
        tv.setLayoutParams(params);
        tv.setText(more);
        tv.setPadding(10, 0, 20, 0);
        tv.setTextColor(Color.parseColor("#3399FF"));
        tv.setTypeface(Typeface.DEFAULT_BOLD);
        tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
        tv.setId(id + 4);
        RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        p.addRule(RelativeLayout.BELOW, id + 3);
        p.addRule(RelativeLayout.CENTER_HORIZONTAL);
        cardViewRelativeLayout.addView(tv, p);
    }


    private void addTextViewData(int id, RelativeLayout cardViewRelativeLayout, String created) {
        TextView tv = new TextView(this);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(
                ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.MATCH_PARENT
        );
        tv.setLayoutParams(params);
        tv.setText(created);
        tv.setPadding(10, 0, 0, 0);
        tv.setTextColor(Color.BLACK);
        tv.setId(id + 3);
        RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        p.addRule(RelativeLayout.BELOW, id + 2);
        cardViewRelativeLayout.addView(tv, p);
    }

    private void addTextViewTeacher(int id, RelativeLayout cardViewRelativeLayout, String teacher) {

        TextView tv = new TextView(this);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(
                ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.MATCH_PARENT
        );
        tv.setLayoutParams(params);
        tv.setText(teacher);
        tv.setPadding(10, 0, 0, 0);
        tv.setTextColor(Color.BLACK);
        tv.setId(id + 2);
        RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        p.addRule(RelativeLayout.BELOW, id + 5);
        cardViewRelativeLayout.addView(tv, p);
    }

    private void addImageView(int id, RelativeLayout relativeLayout, Integer drawable, final Integer lectureId, final Integer videoId) {

        ImageView imageView = new ImageView(this);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.MATCH_PARENT);
        imageView.setLayoutParams(layoutParams);
        imageView.setBackgroundResource(drawable);
        imageView.setId(id + 1);
        imageView.getLayoutParams().height = 200;


        ViewGroup.MarginLayoutParams marginParams = new ViewGroup.MarginLayoutParams(imageView.getLayoutParams());
        marginParams.setMargins(10, 10, 10, 10);
        RelativeLayout.LayoutParams layoutParams1 = new RelativeLayout.LayoutParams(marginParams);
        imageView.setLayoutParams(layoutParams1);
        relativeLayout.addView(imageView);

        ImageView imageView1 = (ImageView) findViewById(id + 1);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoPlaybackActivity = new Intent(LectureActivity.this, VideoPlayer.class);
                Integer lecture = lectureId;
                Integer video = videoId;
                videoPlaybackActivity.putExtra("lectureId", ""+lecture);
                videoPlaybackActivity.putExtra("videoId", ""+video);

                startActivity(videoPlaybackActivity);
            }
        });
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            if( getArguments().getInt(ARG_SECTION_NUMBER) == 1) {
                View rootView = inflater.inflate(R.layout.fragment_pdf, container, false);
                return rootView;
            } else if (getArguments().getInt(ARG_SECTION_NUMBER) == 2){
                View rootView = inflater.inflate(R.layout.fragment_video, container, false);
                return rootView;
            } else if (getArguments().getInt(ARG_SECTION_NUMBER) == 3){
                View rootView = inflater.inflate(R.layout.fragment_audio, container, false);
                return rootView;
            } else {
                View rootView = inflater.inflate(R.layout.fragment_lecture, container, false);
                TextView textView = (TextView) rootView.findViewById(R.id.section_label);
                textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
                return rootView;
            }
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "SECTION 1";
                case 1:
                    return "SECTION 2";
                case 2:
                    return "SECTION 3";
            }
            return null;
        }
    }

}
