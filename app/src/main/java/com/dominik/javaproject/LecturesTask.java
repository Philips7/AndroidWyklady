package com.dominik.javaproject;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import static android.webkit.ConsoleMessage.MessageLevel.LOG;

/**
 * Created by Dominik on 2017-06-04.
 */
public class LecturesTask extends AsyncTask<String, Integer, JSONArray> {

    private Context context;
    private JSONArray responseObject;
    public AsyncResponse delegate;

    public LecturesTask(Context context) {
        this.context = context;
        this.responseObject = null;
        this.delegate = null;
    }

    @Override
    protected JSONArray doInBackground(String... params) {
        JSONArray response = null;
       try {
          response =  getJsonResponse(params[0]);
       } catch (IOException ex) {
           Log.d("LECTURESTASK", ex.getMessage());
       } catch (JSONException ex) {
           Log.d("LECTURESTASK", ex.getMessage());
       } finally {
           return response;
       }


    }


    @Override
    protected void onPostExecute(JSONArray jsonObject) {
        delegate.processFinish(jsonObject);
    }

    private JSONArray getJsonResponse(String urlString)  throws IOException, JSONException {
        HttpURLConnection urlConnection;
        URL url = new URL(urlString);
        urlConnection = (HttpURLConnection) url.openConnection();

        urlConnection.setRequestMethod("GET");
        urlConnection.setReadTimeout(10000);
        urlConnection.setConnectTimeout(15000);
        urlConnection.setDoOutput(true);
        urlConnection.connect();

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(url.openStream()));
        char[] buffer = new char[1024];
        String jsonString = new String();
        StringBuilder sb = new StringBuilder();
        String line;

        while ((line = bufferedReader.readLine()) != null) {
            sb.append(line+"\n");
        }
        bufferedReader.close();
        jsonString = sb.toString();

        System.out.println("JSON: " + jsonString);

        return new JSONArray(jsonString);
    }
}
