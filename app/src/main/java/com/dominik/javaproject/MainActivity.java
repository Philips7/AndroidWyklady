package com.dominik.javaproject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.Image;
import android.os.AsyncTask;
import android.os.PowerManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class MainActivity extends AppCompatActivity implements AsyncResponse {
    ProgressDialog mProgressDialog;


    @Override
    public void processFinish(JSONArray jsonArrayResponse) {

        ArrayList<Lecture> lectures = null;
        if (jsonArrayResponse != null) {
            lectures = jsonArrayToLectures(jsonArrayResponse);
        }

        if (lectures != null) {
            LinearLayout mainLayout = (LinearLayout) findViewById(R.id.linearLayout);
            int viewCounter = 6;
            int cardViewCounter = lectures.size();
            generateCards(mainLayout, viewCounter, cardViewCounter, lectures);
        }
    }


    private ArrayList<Lecture> jsonArrayToLectures(JSONArray jsonArrayResponse) {
        ArrayList<Lecture> lectures = new ArrayList<>();
        String pattern = "MM/dd/yyyy";
        SimpleDateFormat format = new SimpleDateFormat(pattern);

        for (int i = 0; i < jsonArrayResponse.length(); i++) {
            try {
                JSONObject object = (JSONObject) jsonArrayResponse.get(i);
                Lecture lecture = new Lecture();
                lecture.setTeacherName(object.getString("teacherName"));
                lecture.setTeacherSurname(object.getString("teacherSurname"));
                lecture.setTitle(object.getString("name"));
                lecture.setLectureId(object.getInt("lectureId"));
                Date date = format.parse("12/31/2006");
                lecture.setDate(date);
                lectures.add(lecture);
            } catch (JSONException ex) {
                Log.d("MAIN", ex.getMessage());
            } catch (ParseException ex) {
                Log.d("MAIN", ex.getMessage());
            }
        }
        return lectures;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // instantiate it within the onCreate method
        mProgressDialog = new ProgressDialog(MainActivity.this);
        mProgressDialog.setMessage("Donwloading file");
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog.setCancelable(true);

            // execute this when the downloader must be fired
//            final DownloadTask downloadTask2 = new DownloadTask(MainActivity.this);
//            downloadTask2.execute("https://pacific-ocean-71911.herokuapp.com/videos/file/1", "TaylorSwift", ".mpk4", "video", "wyklad1");


        final LecturesTask lecturesTask = new LecturesTask(MainActivity.this);
        lecturesTask.delegate = this;
        lecturesTask.execute("https://pacific-ocean-71911.herokuapp.com/lectures");

        mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                //       downloadTask.cancel(true);
            }
        });

    }

    private void generateCards(LinearLayout mainLayout, int viewCounter, int cardViewCounter, ArrayList<Lecture> lectures) {

        Integer[] drawables = new Integer[3];
        drawables[0] = R.drawable.article;
        drawables[1] = R.drawable.article;
        drawables[2] = R.drawable.article;


        for (int id = 0, j = 0; id < viewCounter * cardViewCounter; id += viewCounter, j++) {
            String teacher = lectures.get(j).getTeacherName() + " " + lectures.get(j).getTeacherSurname();
            String title = lectures.get(j).getTitle();
            String date = lectures.get(j).getDate().toString();
            String lectureId = lectures.get(j).getLectureId().toString();
            createCardView(mainLayout, id, title, teacher, date, lectureId, drawables[j]);
        }
    }

    private void createCardView(LinearLayout mainLinearLayout, int id, String title, String teacher, String date, String lectureId, Integer drawable) {

        CardView cardView = new CardView(this);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(
                ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.MATCH_PARENT
        );
        cardView.setLayoutParams(params);
        cardView.setCardBackgroundColor(Color.parseColor("#FFC6D6C3"));
        cardView.setId(id);
        RelativeLayout cardViewRelativeLayout = new RelativeLayout(this);
        ActionBar.LayoutParams relativeParams = new ActionBar.LayoutParams(
                ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.MATCH_PARENT
        );
        cardView.setContentPadding(15, 15, 15, 15);
        cardView.setMaxCardElevation(15);
        cardView.setCardElevation(9);
        cardView.addView(cardViewRelativeLayout, relativeParams);

        addImageView(id, cardViewRelativeLayout, drawable, lectureId);
        addTextViewTitle(id, cardViewRelativeLayout, title);
        addTextViewTeacher(id, cardViewRelativeLayout, teacher);
        addTextViewData(id, cardViewRelativeLayout, date);
        addTextViewMore(id, cardViewRelativeLayout, "More...");

        mainLinearLayout.addView(cardView);
    }

    private void addTextViewTitle(int id, RelativeLayout cardViewRelativeLayout, String title) {
        TextView tv = new TextView(this);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(
                ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.MATCH_PARENT
        );
        tv.setLayoutParams(params);
        tv.setText(title);
        tv.setPadding(10, 0, 20, 0);
        tv.setTextColor(Color.DKGRAY);
        tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        tv.setTypeface(Typeface.DEFAULT_BOLD);
        tv.setId(id + 5);
        RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        p.addRule(RelativeLayout.BELOW, id + 1);
        p.addRule(RelativeLayout.CENTER_HORIZONTAL, 1);
        p.addRule(RelativeLayout.CENTER_VERTICAL, 1);
        cardViewRelativeLayout.addView(tv, p);
    }

    private void addTextViewMore(int id, RelativeLayout cardViewRelativeLayout, String more) {
        TextView tv = new TextView(this);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(
                ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.MATCH_PARENT
        );
        tv.setLayoutParams(params);
        tv.setText(more);
        tv.setPadding(10, 0, 20, 0);
        tv.setTextColor(Color.parseColor("#3399FF"));
        tv.setTypeface(Typeface.DEFAULT_BOLD);
        tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
        tv.setId(id + 4);
        RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        p.addRule(RelativeLayout.BELOW, id + 3);
        p.addRule(RelativeLayout.CENTER_HORIZONTAL);
        cardViewRelativeLayout.addView(tv, p);
    }


    private void addTextViewData(int id, RelativeLayout cardViewRelativeLayout, String created) {
        TextView tv = new TextView(this);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(
                ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.MATCH_PARENT
        );
        tv.setLayoutParams(params);
        tv.setText(created);
        tv.setPadding(10, 0, 0, 0);
        tv.setTextColor(Color.BLACK);
        tv.setId(id + 3);
        RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        p.addRule(RelativeLayout.BELOW, id + 2);
        cardViewRelativeLayout.addView(tv, p);
    }

    private void addTextViewTeacher(int id, RelativeLayout cardViewRelativeLayout, String teacher) {

        TextView tv = new TextView(this);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(
                ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.MATCH_PARENT
        );
        tv.setLayoutParams(params);
        tv.setText(teacher);
        tv.setPadding(10, 0, 0, 0);
        tv.setTextColor(Color.BLACK);
        tv.setId(id + 2);
        RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        p.addRule(RelativeLayout.BELOW, id + 5);
        cardViewRelativeLayout.addView(tv, p);
    }

    private void addImageView(int id, RelativeLayout relativeLayout, Integer drawable, final String lectureId) {

        ImageView imageView = new ImageView(this);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.MATCH_PARENT);
        imageView.setLayoutParams(layoutParams);
        imageView.setBackgroundResource(drawable);
        imageView.setId(id + 1);
        imageView.getLayoutParams().height = 200;


        ViewGroup.MarginLayoutParams marginParams = new ViewGroup.MarginLayoutParams(imageView.getLayoutParams());
        marginParams.setMargins(10, 10, 10, 10);
        RelativeLayout.LayoutParams layoutParams1 = new RelativeLayout.LayoutParams(marginParams);
        imageView.setLayoutParams(layoutParams1);
        relativeLayout.addView(imageView);

        Integer viewId = id + 1;
        final DownloadTask downloadTask1 = new DownloadTask(MainActivity.this);
        downloadTask1.execute("https://pacific-ocean-71911.herokuapp.com/lectures/file/" + lectureId, "image1", ".jpg", "image", "wyklad" + lectureId, "" + viewId);


        ImageView imageView1 = (ImageView) findViewById(id + 1);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(getBaseContext(), "Click", Toast.LENGTH_LONG).show();

                Intent myIntent = new Intent(MainActivity.this, LectureActivity.class);
                myIntent.putExtra("lectureId", lectureId); //Optional parameters
                MainActivity.this.startActivity(myIntent);
            }
        });


    }

    // usually, subclasses of AsyncTask are declared inside the activity class.
    // that way, you can easily modify the UI thread from here
    private class DownloadTask extends AsyncTask<String, Integer, String> {

        private Context context;
        private PowerManager.WakeLock mWakeLock;
        private String filename;
        private String extension;
        private String type;
        private String directory;
        private String viewFileId;

        public DownloadTask(Context context) {
            this.context = context;
        }

        @Override
        protected String doInBackground(String... sUrl) {
            filename = sUrl[1];
            extension = sUrl[2];
            type = sUrl[3];
            directory = sUrl[4];
            viewFileId = sUrl[5];

            InputStream input = null;
            OutputStream output = null;
            HttpURLConnection connection = null;
            try {
                URL url = new URL(sUrl[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                // expect HTTP 200 OK, so we don't mistakenly save error report
                // instead of the file
                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    return "Server returned HTTP " + connection.getResponseCode()
                            + " " + connection.getResponseMessage();
                }

                // this will be useful to display download percentage
                // might be -1: server did not report the length
                int fileLength = connection.getContentLength();

                // download the file
                input = connection.getInputStream();
                output = new FileOutputStream("/sdcard/appJava/" + directory + "/" + type + "/" + filename + extension);

                byte data[] = new byte[4096];
                long total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    // allow canceling with back button
                    if (isCancelled()) {
                        input.close();
                        File file = new File("/sdcard/appJava/" + directory + "/" + type + "/" + filename + extension);
                        if (file.exists()) {
                            file.delete();
                        }
                        return null;
                    }
                    total += count;
                    // publishing the progress....
                    if (fileLength > 0) // only if total length is known
                        publishProgress((int) (total * 100 / fileLength));
                    output.write(data, 0, count);
                }
            } catch (Exception e) {
                return e.toString();
            } finally {
                try {
                    if (output != null)
                        output.close();
                    if (input != null)
                        input.close();
                } catch (IOException ignored) {
                }

                if (connection != null)
                    connection.disconnect();
            }
            if (viewFileId != null) {
                MainActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Integer id = Integer.parseInt(viewFileId);
                        ImageView iv = (ImageView) findViewById(id);
                        Bitmap bmp = BitmapFactory.decodeFile("/sdcard/appJava/" + directory + "/" + type + "/" + filename + extension);
                        Bitmap bmpimg = Bitmap.createScaledBitmap(bmp, iv.getWidth(), iv.getHeight(), true);
                        iv.setImageBitmap(bmpimg);

                    }
                });

            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // take CPU lock to prevent CPU from going off if the user
            // presses the power button during download
            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                    getClass().getName());
            mWakeLock.acquire();
           // mProgressDialog.show();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            // if we get here, length is known, now set indeterminate to false
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setMax(100);
            mProgressDialog.setProgress(progress[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            mWakeLock.release();
            //mProgressDialog.dismiss();
            if (result != null) {
                File file = new File("/sdcard/appJava/" + directory + "/" + type + "/" + filename + extension);
                if (file.exists()) {
                    file.delete();
                }
                Toast.makeText(context, "Download error: " + result, Toast.LENGTH_LONG).show();
            } else{
//                Toast.makeText(context, "File downloaded", Toast.LENGTH_SHORT).show();
            }
        }
    }


}
