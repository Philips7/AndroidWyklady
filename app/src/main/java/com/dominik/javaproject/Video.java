package com.dominik.javaproject;

import java.util.Date;

/**
 * Created by Dominik on 2017-06-09.
 */

public class Video {

    private Integer videoId;
    private String filename;
    private String description;
    private String extension;
    private Integer lectureId;
    private Date date;

    public Video() {
    }

    public Video(Integer videoId, String filename, String description, String extension, Integer lectureId, Date date) {
        this.videoId = videoId;
        this.filename = filename;
        this.description = description;
        this.extension = extension;
        this.lectureId = lectureId;
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getVideoId() {
        return videoId;
    }

    public void setVideoId(Integer videoId) {
        this.videoId = videoId;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public Integer getLectureId() {
        return lectureId;
    }

    public void setLectureId(Integer lectureId) {
        this.lectureId = lectureId;
    }
}
