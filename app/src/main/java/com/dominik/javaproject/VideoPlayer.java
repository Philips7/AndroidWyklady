package com.dominik.javaproject;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.MediaController;
import android.widget.VideoView;

import java.io.File;

/**
 * Created by Dominik on 2017-06-10.
 */


public class VideoPlayer extends Activity implements MediaPlayer.OnCompletionListener,MediaPlayer.OnPreparedListener,View.OnTouchListener {

    private VideoView mVV;
    private MediaController mediaC;
    private int stopPosition;
    View v;
    String lectureId;
    String videoId;

    @Override
    public void onCreate(Bundle b) {
        super.onCreate(b);

        setContentView(R.layout.video_player);


        mediaC = new MediaController(this);
        stopPosition = 0;
        String resourceName = null;
        lectureId = null;
        videoId = null;
        Bundle e = getIntent().getExtras();
        if (e!=null) {
            lectureId = e.getString("lectureId");
            videoId = e.getString("videoId");
            resourceName = "video" + lectureId + "_" + videoId;
        }

        boolean fileExists = videoExists(resourceName);
//        if (fileExists == false) {
//            final MainActivity.DownloadTask downloadTask1 = new MainActivity.DownloadTask(VideoPlayer.this);
//            downloadTask1.execute("https://pacific-ocean-71911.herokuapp.com/video/file/" + lectureId, "image1", ".jpg", "image", "wyklad" + lectureId, ""+viewId);
//TODO
//        }

        mVV = (VideoView)findViewById(R.id.videoView);
        mVV.setOnCompletionListener(this);
        mVV.setOnPreparedListener(this);
        mVV.setOnTouchListener(this);

        if (!playFileRes(resourceName)) return;

        mVV.setMediaController(mediaC);
        mediaC.setAnchorView(mVV.getRootView());
        mVV.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mVV.start();
                mediaC.show(900000000);
            }
        });
        mVV.start();
    }

    private boolean videoExists(String resourceName) {
        String filePathString = "/sdcard/appJava/" + "wyklad" + lectureId + "/video/" + resourceName + ".mp4";
        File f = new File(filePathString);
        if(f.exists() ) {
            System.out.println("tak");
            return true;
        }
        return false;
    }

    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        String resourceName = null;
        Bundle e = getIntent().getExtras();
        if (e != null) {
            lectureId = e.getString("lectureId");
            String videoId = e.getString("videoId");
            resourceName = "video" + lectureId + "_" + videoId;
        }
        playFileRes(resourceName);
    }

    private boolean playFileRes(String resourceName) {
        if (resourceName == null) {
            stopPlaying();
            return false;
        } else {
//            mVV.setVideoPath(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/videos/" + resourceName + ".3gp");
            mVV.setVideoPath("/sdcard/appJava/" + "wyklad" + lectureId + "/video/" + resourceName + ".mp4");
            return true;
        }
    }

    public void stopPlaying() {
        mVV.stopPlayback();
        //this.finish();
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        finish();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        mediaC.show(40000);
        return true;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        mp.setLooping(true);
    }

}
